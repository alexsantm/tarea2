//
//  DetallePeliculasViewController.swift
//  Tarea2
//
//  Created by macbook on 1/29/21.
//  Copyright © 2021 macbook. All rights reserved.
//

import UIKit

class DetallePeliculasViewController: UIViewController {

    @IBOutlet weak var tituloPelicula: UILabel!
    @IBOutlet weak var detallePelicula: UITextView!
    @IBOutlet weak var fechaEstrenoPelicula: UILabel!
    
    var paramReceptor:[String:String]?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        guard let paramDatos = paramReceptor else {return}
       let nombrePelicula = paramDatos["nombrePelicula"] as? String ?? ""
       let descripcionPelicula = paramDatos["descripcionPelicula"] as? String ?? ""
       let fechaEstrenoPelicula = paramDatos["fechaEstrenoPelicula"] as? String ?? ""

   
        self.tituloPelicula.text = nombrePelicula
        self.detallePelicula.text = descripcionPelicula
        self.fechaEstrenoPelicula.text = fechaEstrenoPelicula

    }

}
