//
//  AddViewController.swift
//  Tarea2
//
//  Created by macbook on 1/29/21.
//  Copyright © 2021 macbook. All rights reserved.
//

import UIKit

class AddPeliculasViewController: UIViewController {

    @IBOutlet weak var nombrePelicula: UITextField!
    @IBOutlet weak var descripcionPelicula: UITextView!
    @IBOutlet weak var fechaEstrenoPelicula: UIDatePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
//        if segue.identifier == "goToPeliculas" {
//            let objDestino = segue.destination as! PeliculasViewController
//            
//            fechaEstrenoPelicula.datePickerMode = .date
//            let dateFormatter = DateFormatter()
//            dateFormatter.dateFormat = "dd-MM-YYYY"
//            var strDate = dateFormatter.string(from: fechaEstrenoPelicula.date)
//            let fechaPelicula = strDate
//
//            objDestino.nombre = nombrePelicula.text ?? "default "
//            objDestino.descripcion = descripcionPelicula.text ?? "default "
//            objDestino.fecha = fechaPelicula as String
//        }
   }
    
    @IBAction func grabarDatos(_ sender: Any) {
        
        fechaEstrenoPelicula.datePickerMode = .date
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-YYYY"
        var strDate = dateFormatter.string(from: fechaEstrenoPelicula.date)
        let fechaPelicula = strDate
        
        guard let nomPelicula = self.nombrePelicula.text, let descPelicula = self.descripcionPelicula.text else { return }
        let newPelicula = ["nombrePelicula": nomPelicula, "descripcionPelicula": descPelicula, "fechaEstrenoPelicula": fechaPelicula]
        Peliculas.shared.arrayPeliculas.append(newPelicula)
        print("arrayPeliculas a enviar", Peliculas.shared.arrayPeliculas)

        dismiss(animated: true){
            
        }
    }
    
}
