//
//  ViewController.swift
//  Tarea2
//
//  Created by macbook on 1/29/21.
//  Copyright © 2021 macbook. All rights reserved.
//

import UIKit

class PeliculasViewController: UIViewController {

    @IBOutlet weak var PeliculasTableView: UITableView!
    
    var arrayPeliculas = [[String:String]]()  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("arrayPeliculas 0 - Origen", arrayPeliculas)
    }
    
    override func viewWillAppear(_ animated: Bool) {
  
        // Do any additional setup after loading the view.
        arrayPeliculas = Peliculas.shared.arrayPeliculas
        PeliculasTableView.reloadData()
        print("arrayPeliculas 1 - Origen", arrayPeliculas)
    }
    
      override func viewDidAppear(_ animated: Bool) {
    
          // Do any additional setup after loading the view.
          arrayPeliculas = Peliculas.shared.arrayPeliculas
          print("arrayPeliculas 2 - Origen", arrayPeliculas)
      }
    
     override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if segue.identifier == "goToDetalle"{
            guard let indexSelected = PeliculasTableView.indexPathForSelectedRow else {return}
            let objDetalleDestino = segue.destination as? DetallePeliculasViewController
            objDetalleDestino?.paramReceptor = arrayPeliculas[indexSelected.row]
        }
    }
    
}

extension PeliculasViewController:UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        // Validacion que muestra mensaje de no hay peliculas
        var numOfSections: Int = 0
        if self.arrayPeliculas.count > 0
        {
            tableView.separatorStyle = .singleLine
            numOfSections            = 1
            tableView.backgroundView = nil
        }
        else
        {
            let noDataLabel: UILabel  = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: tableView.bounds.size.height))
            noDataLabel.text          = "Haga click en ADD para añadir una pelicula"
            noDataLabel.textColor     = UIColor.black
            noDataLabel.textAlignment = .center
            tableView.backgroundView  = noDataLabel
            tableView.separatorStyle  = .none
        }
        return numOfSections
        
        //return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return arrayPeliculas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "celdaPelicula", for: indexPath)
        let fila = indexPath.row
        
        let nombrePelicula = arrayPeliculas[fila]["nombrePelicula"] ??  ""
        let descripcionPelicula = arrayPeliculas[indexPath.row]["descripcionPelicula"] ??  ""
        let fechaPelicula = arrayPeliculas[indexPath.row]["fechaEstrenoPelicula"] ??  ""
        
        cell.textLabel?.text = nombrePelicula
        cell.imageView?.image = UIImage(named: "pelicula")
        cell.detailTextLabel?.text = fechaPelicula
       
        return cell
    }
    
    private func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> String? {
        return "seccion No. \(section)"
    }
    
}
